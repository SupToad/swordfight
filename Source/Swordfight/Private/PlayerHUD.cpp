// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerHUD.h"


void UPlayerHUD::SetPlayerHealth(float CurrentHealth, float MaxHealth)
{
	if (PlayerHealth)
	{
		PlayerHealth->SetPercent(CurrentHealth / MaxHealth);
	}
}

void UPlayerHUD::SetOpponentHealth(float CurrentHealth, float MaxHealth)
{
	if (OpponentHealth)
	{
		OpponentHealth->SetPercent(CurrentHealth / MaxHealth);
	}
}