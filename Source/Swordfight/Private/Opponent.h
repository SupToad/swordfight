// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
//#include "SwordPlayer.h"
//#include "Fight.h"
#include "Opponent.generated.h"

UDELEGATE()
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FAttack);

UCLASS()
class AOpponent : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AOpponent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USceneComponent* VisibleComponent;

	UPROPERTY(BlueprintAssignable)
	FAttack OnAttack;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/* Health */

	UPROPERTY(EditAnywhere, Category = "Health")
	float MaxHealth;
	UPROPERTY(EditAnywhere, Category = "Health")
	float CurrentHealth;

	bool CanBeDamaged;
	bool CanBeDamagedUp;

	void ReceiveDamage(float Amount);

	/* Attacks */

	FTimerHandle CountdownTimerHandle;
	void Attack();
};
