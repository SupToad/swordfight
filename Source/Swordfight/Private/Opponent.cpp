// Fill out your copyright notice in the Description page of Project Settings.


#include "Opponent.h"

// Sets default values
AOpponent::AOpponent()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create a dummy root component we can attach things to.
	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisibleComponent"));
	VisibleComponent->SetupAttachment(RootComponent);

	MaxHealth = 100.0f;
	CurrentHealth = MaxHealth;

	CanBeDamaged = true;
	CanBeDamagedUp = true;
}

// Called when the game starts or when spawned
void AOpponent::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(CountdownTimerHandle, this, &AOpponent::Attack, 2.0f, false);
}

// Called every frame
void AOpponent::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AOpponent::ReceiveDamage(float Amount)
{
	CurrentHealth -= Amount;
	FString TheFloatStr = FString::SanitizeFloat(CurrentHealth);
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Blue, *TheFloatStr);

}

void AOpponent::Attack()
{

	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, TEXT("Attack!"));
	OnAttack.Broadcast();

}
