// Fill out your copyright notice in the Description page of Project Settings.


#include "SwordPlayer.h"
#include "Camera/CameraComponent.h"
#include "InputMappingContext.h"
#include "EnhancedInputSubsystems.h"
#include "EnhancedInput/Public/EnhancedInputComponent.h"
#include "MyInputConfigData.h"
#include "InputActionValue.h"

// Sets default values
ASwordPlayer::ASwordPlayer()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	// Set this pawn to be controlled by the lowest-numbered player
	AutoPossessPlayer = EAutoReceiveInput::Player0;

    // Create a dummy root component we can attach things to.
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
    // Create a camera and a visible object
    UCameraComponent* OurCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("OurCamera"));
    OurVisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("OurVisibleComponent"));
    // Attach our camera and visible object to our root component. Offset and rotate the camera.
    OurCamera->SetupAttachment(RootComponent);
    OurCamera->SetRelativeLocation(FVector(-450.0f, 0.0f, 250.0f));
    OurCamera->SetRelativeRotation(FRotator(-22.5f, 0.0f, 0.0f));
    OurVisibleComponent->SetupAttachment(RootComponent);

    DodgeTime = 0.6f;
    CanAct = true;

    MaxHealth = 100.0f;
    CurrentHealth = MaxHealth;

    //HUD
    PlayerHUDClass = nullptr;
    PlayerHUD = nullptr;

    PlayerPosition = EPosition::Neutral;
}

// Called when the game starts or when spawned
void ASwordPlayer::BeginPlay()
{
	Super::BeginPlay();
	
    if (PlayerHUDClass)
    {
        APlayerController* PC = GetController<APlayerController>();
        check(PC);
        PlayerHUD = CreateWidget<UPlayerHUD>(PC, PlayerHUDClass);
        check(PlayerHUD);
        PlayerHUD->AddToPlayerScreen();

        PlayerHUD->SetPlayerHealth(CurrentHealth, MaxHealth);
    }
}

void ASwordPlayer::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
    if (PlayerHUD)
    {
        PlayerHUD->RemoveFromParent();
        PlayerHUD = nullptr;
    }
}

// Called every frame
void ASwordPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASwordPlayer::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
    // Get the player controller
    APlayerController* PC = Cast<APlayerController>(GetController());

    // Get the local player subsystem
    UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PC->GetLocalPlayer());
    // Clear out existing mapping, and add our mapping
    Subsystem->ClearAllMappings();
    Subsystem->AddMappingContext(InputMapping, 0);

    // Get the EnhancedInputComponent
    UEnhancedInputComponent* PEI = Cast<UEnhancedInputComponent>(PlayerInputComponent);
    // Bind the actions
    PEI->BindAction(InputActions->InputMove, ETriggerEvent::Triggered, this, &ASwordPlayer::Move);
    PEI->BindAction(InputActions->InputAttack, ETriggerEvent::Triggered, this, &ASwordPlayer::Attack);
}

void ASwordPlayer::Move(const FInputActionValue& Value)
{
    if (Controller != nullptr)
    {
        if (CanAct)
        {
            const FVector2D MoveValue = Value.Get<FVector2D>();
            /*FString TheFloatStr = FString::SanitizeFloat(MoveValue.X);
            if (GEngine)
                GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Yellow, *TheFloatStr);*/

            if (MoveValue.X > 0.0)
                DodgeRight();
            else if (MoveValue.X < 0.0)
                DodgeLeft();
            else if (MoveValue.Y < 0.0)
                DodgeDown();
            else
                PlayerPosition = EPosition::Neutral;
            CanAct = false;
            GetWorldTimerManager().SetTimer(CountdownTimerHandle, this, &ASwordPlayer::CountdownHasFinished, DodgeTime, false);
        }
    }
}

void ASwordPlayer::DodgeLeft()
{
    PlayerPosition = EPosition::Left;
    OurVisibleComponent->SetRelativeLocation(FVector(0.0f, -100.0f, 0.0f));
}
void ASwordPlayer::DodgeRight()
{
    PlayerPosition = EPosition::Right;
    OurVisibleComponent->SetRelativeLocation(FVector(0.0f, 100.0f, 0.0f));
}
void ASwordPlayer::DodgeDown()
{
    PlayerPosition = EPosition::Down;
    OurVisibleComponent->SetRelativeLocation(FVector(0.0f, 0.0f, -25.0f));
    OurVisibleComponent->SetRelativeScale3D(FVector(1.0f, 1.0f, 0.5f));
}

void ASwordPlayer::Attack(const FInputActionValue& Value)
{
    if (CanAct)
    {
        if (GEngine)
            GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Blue, TEXT("Can Attack"));
        OnPlayerAttack.Broadcast();

        if (SlashEffect)
        {
            UNiagaraComponent* NiagaraComp = UNiagaraFunctionLibrary::SpawnSystemAttached(SlashEffect, RootComponent, NAME_None, FVector(0.f), FRotator(0.f), EAttachLocation::Type::KeepRelativeOffset, true);
        }
    }
}


void ASwordPlayer::CountdownHasFinished()
{
    if (GEngine)
        GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Blue, TEXT("Timer Over"));
    PlayerPosition = EPosition::Neutral;
    OurVisibleComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
    OurVisibleComponent->SetRelativeScale3D(FVector(1.0f, 1.0f, 1.0f));
    CanAct = true;
    GetWorldTimerManager().ClearTimer(CountdownTimerHandle);
}


void ASwordPlayer::ReceiveDamage(float Amount)
{
    CurrentHealth -= Amount;
    FString TheFloatStr = FString::SanitizeFloat(CurrentHealth);
    if (GEngine)
        GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Blue, *TheFloatStr);
    PlayerHUD->SetPlayerHealth(CurrentHealth, MaxHealth);

}


void ASwordPlayer::UpdateOpponentHUD(float current, float max)
{
    FString TheFloatStr = FString::SanitizeFloat(current);
    if (GEngine)
        GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, *TheFloatStr);
    PlayerHUD->SetOpponentHealth(current, max);
}