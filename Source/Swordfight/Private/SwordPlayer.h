// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "InputMappingContext.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "PlayerHUD.h"
#include "Blueprint/UserWidget.h"
#include "SwordPlayer.generated.h"

UENUM(BlueprintType)
enum class EPosition : uint8
{
	Neutral	UMETA(DisplayName = "Neutral"),
	Left	UMETA(DisplayName = "Left"),
	Right	UMETA(DisplayName = "Right"),
	Down	UMETA(DisplayName = "Down"),
};

UDELEGATE()
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerAttack);

UCLASS()
class ASwordPlayer : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASwordPlayer();

	UPROPERTY(EditAnywhere)
	USceneComponent* OurVisibleComponent;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(EditAnywhere, Category = "Input")
	class UInputMappingContext* InputMapping;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Enhanced Input")
	class UMyInputConfigData* InputActions;

	// Handle move input
	void Move(const FInputActionValue& Value);

	// Handle attack input
	void Attack(const FInputActionValue& Value);

	// Stores current player position
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EPosition PlayerPosition;

	/* Health */

	UPROPERTY(EditAnywhere, Category = "Health")
	float MaxHealth;
	UPROPERTY(EditAnywhere, Category = "Health")
	float CurrentHealth;

	void ReceiveDamage(float Amount);

	/* Dodge System */

	void DodgeLeft();
	void DodgeRight();
	void DodgeDown();

	float DodgeTime;
	void CountdownHasFinished();
	FTimerHandle CountdownTimerHandle;

	bool CanAct;

	/* Attack */

	UPROPERTY(BlueprintAssignable)
	FPlayerAttack OnPlayerAttack;

	UPROPERTY(EditAnywhere, Category = "VFX")
	UNiagaraSystem* SlashEffect;

	/* HUD */

	UPROPERTY(EditAnywhere, Category = "HUD")
	TSubclassOf<class UPlayerHUD> PlayerHUDClass;

	UPROPERTY()
	class UPlayerHUD* PlayerHUD;

	void UpdateOpponentHUD(float current, float max);
};
