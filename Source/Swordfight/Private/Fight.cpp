// Fill out your copyright notice in the Description page of Project Settings.


#include "Fight.h"

// Sets default values
AFight::AFight()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFight::BeginPlay()
{
	Super::BeginPlay();

	if (SwordPlayer)
	{
		SwordPlayer->OnPlayerAttack.AddDynamic(this, &AFight::PlayerAttacks);
		//if (Opponent)
			//SwordPlayer->UpdateOpponentHUD(Opponent->CurrentHealth, Opponent->MaxHealth);
	}
	if (Opponent)
	{
		Opponent->OnAttack.AddDynamic(this, &AFight::OpponentAttacks);
	}
}

// Called every frame
void AFight::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Delegate when player attacks
void AFight::PlayerAttacks()
{
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Blue, TEXT("Player Attacks"));
	if (Opponent->CanBeDamaged)
	{
		Opponent->ReceiveDamage(5.0f);
		SwordPlayer->UpdateOpponentHUD(Opponent->CurrentHealth, Opponent->MaxHealth);
	}
}

// Delegate when opponent attacks
void AFight::OpponentAttacks()
{
	if (GEngine)
		GEngine->AddOnScreenDebugMessage(-1, 1.0f, FColor::Red, TEXT("Opponent Attacks"));
	if (SwordPlayer->PlayerPosition == EPosition::Neutral)
	{
		SwordPlayer->ReceiveDamage(15.0f);
	}
}
