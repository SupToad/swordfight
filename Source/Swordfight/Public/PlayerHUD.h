// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Components/ProgressBar.h"
#include "PlayerHUD.generated.h"

/**
 * 
 */
UCLASS(Abstract)
class SWORDFIGHT_API UPlayerHUD : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void SetPlayerHealth(float CurrentHealth, float MaxHealth);

	void SetOpponentHealth(float CurrentHealth, float MaxHealth);

	UPROPERTY(EditAnywhere, meta=(BindWidget))
	class UProgressBar* PlayerHealth;

	UPROPERTY(EditAnywhere, meta = (BindWidget))
	class UProgressBar* OpponentHealth;
};
