// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SwordPlayer.h"
#include "Opponent.h"
#include "Fight.generated.h"

UCLASS()
class SWORDFIGHT_API AFight : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFight();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	ASwordPlayer* SwordPlayer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AOpponent* Opponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/* Delegates */
	
	UFUNCTION()
	void PlayerAttacks();

	UFUNCTION()
	void OpponentAttacks();
};
