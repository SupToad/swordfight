// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SwordfightGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SWORDFIGHT_API ASwordfightGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
